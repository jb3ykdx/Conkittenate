import { PIXELS_PER_CHUNK } from './Chunk.js';

import Player from './Player.js';

export default class LocalPlayer extends Player {
	update() {
		super.update();

		if (this.y < 0) {
			this.chunkUpdate = this.chunkUpdate + 'north';
		}
		if (this.y >= PIXELS_PER_CHUNK) {
			this.chunkUpdate = this.chunkUpdate + 'south';
		}

		if (this.x < 0) {
			this.chunkUpdate = this.chunkUpdate + 'west';
		}
		if (this.x >= PIXELS_PER_CHUNK) {
			this.chunkUpdate = this.chunkUpdate + 'east';
		}
	}
}
