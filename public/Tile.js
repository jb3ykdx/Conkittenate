export const TILE_IMG          = 'tiles';
export const TILE_IMG_RESOURCE = 'assets/tiles.png';

export const LAND_TILE = 3;
export const AIR_TILE  = 51;

export const PIXELS_PER_TILE = 32;
