import { PIXELS_PER_CHUNK }          from './Chunk.js';
import { AIR_TILE, PIXELS_PER_TILE } from './Tile.js';

export const PLAYER_IMG          = 'cat';
export const PLAYER_IMG_RESOURCE = 'assets/cat.png';
export const PLAYER_FRAME_SIZE   = 24;

export const JUMP_POWER = 300;
export const WALK_POWER = 150;
export const DAMPENER = 0.6;

export default class Player extends Phaser.Physics.Arcade.Sprite {
	constructor(
		input,
		scene,
		chunkX = 0,
		chunkY = 0,
		x = Math.random() * PIXELS_PER_CHUNK,
		y = Math.random() * PIXELS_PER_CHUNK
	) {
		let chunk = scene.level.getChunk(chunkX, chunkY);
		let levelData = chunk.data;

		for (; y >= 0; y -= PIXELS_PER_TILE) {
			let tileIndexX = Math.floor(x / PIXELS_PER_TILE);
			let tileIndexY = Math.floor(y / PIXELS_PER_TILE);
			if(levelData[tileIndexX][tileIndexY] == AIR_TILE) {
				break;
			}
		}

		if (y < 0) {
			for (y += PIXELS_PER_TILE; y < PIXELS_PER_CHUNK; y += PIXELS_PER_TILE) {
				let tileIndexX = Math.floor(x / PIXELS_PER_TILE);
				let tileIndexY = Math.floor(y / PIXELS_PER_TILE);
				if(levelData[tileIndexX][tileIndexY] == AIR_TILE) {
					break;
				}
			}
		}

		super(scene, x, y, PLAYER_IMG);

		scene.add.existing(this);
		scene.physics.add.existing(this);

		this.chunk = chunk;
		this.chunkX = chunkX;
		this.chunkY = chunkY;
		this.input = input;
		this.chunkUpdate = '';
	}

	update() {
		if (this.input.getUp() && this.body.onFloor()) {
			this.body.velocity.y = -JUMP_POWER;
		}
		if (this.input.getLeft()) {
			this.body.velocity.x -= WALK_POWER;
		}
		if (this.input.getRight()) {
			this.body.velocity.x += WALK_POWER;
		}
		this.body.velocity.x *= DAMPENER;
	}
}
