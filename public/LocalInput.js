export const CURRENT_FRAME = -1;

export default class LocalInput {
	constructor(scene) {
		this.scene    = scene;
		this.keyUp    = scene.input.keyboard.addKey(Phaser.Input.Keyboard.KeyCodes.UP   );
		this.keyDown  = scene.input.keyboard.addKey(Phaser.Input.Keyboard.KeyCodes.DOWN );
		this.keyLeft  = scene.input.keyboard.addKey(Phaser.Input.Keyboard.KeyCodes.LEFT );
		this.keyRight = scene.input.keyboard.addKey(Phaser.Input.Keyboard.KeyCodes.RIGHT);
	}

	getUp   (frame = CURRENT_FRAME) {
		return this.keyUp   .isDown;
	}

	getDown (frame = CURRENT_FRAME) {
		return this.keyDown .isDown;
	}

	getLeft (frame = CURRENT_FRAME) {
		return this.keyLeft .isDown;
	}

	getRight(frame = CURRENT_FRAME) {
		return this.keyRight.isDown;
	}
}
