import { ARC_CHUNK_LENGTH, ARC_RADIUS } from './Arc.js';

import { simplex } from './Simplex.js';

import Arc     from './Arc.js';
import Segment from './Segment.js';

export default class Level {
	constructor(seed = Date.now()) {
		this.seed         = seed;
		simplex.generator = new SimplexNoise(seed);

		this.width        = ARC_CHUNK_LENGTH;
		this.height       = ARC_CHUNK_LENGTH;

		this.arcsX        = [new Arc(0, ARC_CHUNK_LENGTH, ARC_RADIUS,
			                     0, 0, 0, 2 * Math.PI)];
		this.arcsY        = [new Arc(0, ARC_CHUNK_LENGTH, ARC_RADIUS,
			                     0, 0, 0, 2 * Math.PI)];
	
		this.segments     = [[new Segment(this, this.arcsX[0], this.arcsY[0])]];
		
		this.segments[0][0].constructChunks();
	}

	getChunk(chunkX, chunkY, getData = true) {
		while (chunkX < 0) {
			chunkX += this.width;
		}
		while (chunkY < 0) {
			chunkY += this.height;
		}
		while (chunkX >= this.width) {
			chunkX -= this.width;
		}
		while (chunkY >= this.height) {
			chunkY -= this.height;
		}
		
		let indexX = 0;
		while (chunkX >= this.arcsX[indexX].endChunk) {
			indexX++;
		}
		let indexY = 0;
		while (chunkY >= this.arcsY[indexY].endChunk) {
			indexY++;
		}

		return this.segments[indexX][indexY].getChunk(chunkX, chunkY, getData);
	}
}
