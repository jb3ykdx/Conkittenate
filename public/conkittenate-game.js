import { GRAVITY } from './GameScene.js';

import GameScene from './GameScene.js';

export let gameScene = new GameScene();

export let config = {
	backgroundColor: '#00afaf',
	physics: {
		default: 'arcade',
		arcade: {
			gravity:   { y: GRAVITY }
		}
	},
	render: {
		pixelArt: true
	},
	scale: {
		mode:   Phaser.Scale.RESIZE,
		parent: 'conkittenate-div',
		width:  '100%',
		height: '100%',
	},
	scene: gameScene
};

export let game = new Phaser.Game(config);
