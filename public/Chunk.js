import { PIXELS_PER_TILE } from './Tile.js';

export const TILES_PER_CHUNK = 16;
export const PIXELS_PER_CHUNK = PIXELS_PER_TILE * TILES_PER_CHUNK;

export const NEIGHBORS = {
	'northwest': { x: -1, y: -1 },
	'north':     { x:  0, y: -1 },
	'northeast': { x:  1, y: -1 },
	'west':      { x: -1, y:  0 },
	'this':      { x:  0, y:  0 },
	'east':      { x:  1, y:  0 },
	'southwest': { x: -1, y:  1 },
	'south':     { x:  0, y:  1 },
	'southeast': { x:  1, y:  1 }
};

export const OPPOSITES = {
	'northwest': 'southeast',
	'north':     'south',
	'northeast': 'southwest',
	'west':      'east',
	'this':      'this',
	'east':      'west',
	'southwest': 'northeast',
	'south':     'north',
	'southeast': 'northwest'
};

export default class Chunk {
	constructor(segment, chunkX, chunkY, data, neighbors = {}) {
		this.segment   = segment;
		this.chunkX    = chunkX;
		this.chunkY    = chunkY;
		this.data      = data;
		this.neighbors = {};
	}

	getData() {
		if (!this.data) {
			this.segment.generateChunk(this.chunkX, this.chunkY);
		}

		return this.data;
	}
}
