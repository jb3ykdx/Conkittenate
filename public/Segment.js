import { TILES_PER_CHUNK, NEIGHBORS, OPPOSITES }         from './Chunk.js';
import { NOISE_THRESHOLD, NOISE_SCALE_X, NOISE_SCALE_Y } from './Simplex.js';
import { AIR_TILE, LAND_TILE }                           from './Tile.js';

import { simplex } from './Simplex.js';

import Arc   from './Arc.js';
import Chunk from './Chunk.js';

export default class Segment {
	constructor(level, arcX, arcY) {
		this.level = level;
		this.arcX  = arcX;
		this.arcY  = arcY;
	}

	constructChunks() {
		this.chunks = {};

		for (let x = this.arcX.startChunk; x < this.arcX.endChunk; x++) {
			this.chunks[x] = {};
			for (let y = this.arcY.startChunk; y < this.arcY.endChunk; y++) {
				this.chunks[x][y] = new Chunk(this, x, y);
			}
		}

		this.populateChunkNeighbors();
	}

	populateChunkNeighbors() {
		let borderChunks = {};
		for (let x = this.arcX.startChunk; x < this.arcX.endChunk; x++) {
			for (let y = this.arcY.startChunk; y < this.arcY.endChunk; y++) {
				let centerChunk = this.chunks[x][y];
				for (let neighbor in NEIGHBORS) {
					if (!centerChunk.neighbors[neighbor]) {
						let offset    = NEIGHBORS[neighbor];
						let neighborX = x + offset.x;
						let neighborY = y + offset.y;

						if (!borderChunks[neighborX]) {
							borderChunks[neighborX] = {};
						}

						let neighborChunk;

						if (neighborX >= this.arcX.startChunk &&
							neighborX < this.arcX.endChunk &&
							neighborY >= this.arcY.startChunk &&
							neighborY < this.arcY.endChunk) {

							neighborChunk =
								this.chunks[neighborX][neighborY];
						} else if (borderChunks[neighborX][neighborY]) {
							neighborChunk =
								borderChunks[neighborX][neighborY];
						} else {
							neighborChunk = this.level.getChunk(
								neighborX,
								neighborY,
								false
							);
							borderChunks[neighborX][neighborY] =
								neighborChunk;
						}

						centerChunk.neighbors[neighbor] = neighborChunk;
						neighborChunk.neighbors[OPPOSITES[neighbor]] =
							centerChunk;
					}
				}
			}
		}
	}

	getChunk(chunkX, chunkY, getData = true) {
		if (getData && !this.chunks[chunkX][chunkY].data) {
				this.generateChunk(chunkX, chunkY);
		}

		return this.chunks[chunkX][chunkY];
	}

	generateChunk(chunkX, chunkY) {
		let chunkData = [];
		for (let tileX = 0; tileX < TILES_PER_CHUNK; tileX++) {
			chunkData[tileX] = [];
			for (let tileY = 0; tileY < TILES_PER_CHUNK; tileY++) {
				let arcPortionX = (chunkX - this.arcX.startChunk +
					tileX / TILES_PER_CHUNK) / this.arcX.chunkLength;
				let arcPortionY = (chunkY - this.arcY.startChunk +
					tileY / TILES_PER_CHUNK) / this.arcY.chunkLength;

				let angleX = arcPortionX * this.arcX.arcLength +
					this.arcX.startAngle;
				let angleY = arcPortionY * this.arcY.arcLength +
					this.arcY.startAngle;

				let xCircleX = Math.cos(angleX) * this.arcX.radius;
				let xCircleY = Math.sin(angleX) * this.arcX.radius;

				let yCircleX = Math.cos(angleY) * this.arcY.radius;
				let yCircleY = Math.cos(angleY) * this.arcY.radius;

				let noise = simplex.generator.noise4D(
					(xCircleX + this.arcX.offsetX) * NOISE_SCALE_X,
					(xCircleY + this.arcX.offsetY) * NOISE_SCALE_X,
					(yCircleX + this.arcY.offsetX) * NOISE_SCALE_Y,
					(yCircleY + this.arcY.offsetY) * NOISE_SCALE_Y
				);

				chunkData[tileX][tileY] =
					(noise > NOISE_THRESHOLD) ? LAND_TILE : AIR_TILE;
			}
		}
		
		this.chunks[chunkX][chunkY].data = chunkData;
	}
}
