export const NOISE_THRESHOLD = 0.125;
export const NOISE_SCALE_X = 0.5;
export const NOISE_SCALE_Y = 0.25;

export default class Simplex {
	constructor(generator) {
		this.generator = generator;
	}
}

export const simplex = new Simplex();
