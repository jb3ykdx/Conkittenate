export const ARC_CHUNK_LENGTH = 4;
export const ARC_RADIUS       = 4;

export default class Arc {
	constructor(startChunk, endChunk, radius, offsetX, offsetY, startAngle, endAngle) {
		this.startChunk  = startChunk;
		this.endChunk    = endChunk;
		this.radius      = radius;
		this.offsetX     = offsetX;
		this.offsetY     = offsetY;
		this.startAngle  = startAngle;
		this.endAngle    = endAngle;
		this.chunkLength = endChunk - startChunk;
		this.arcLength   = endAngle - startAngle;
	}
}
