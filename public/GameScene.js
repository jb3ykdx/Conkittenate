import { TILES_PER_CHUNK, PIXELS_PER_CHUNK, NEIGHBORS }            from './Chunk.js';
import { PLAYER_IMG, PLAYER_IMG_RESOURCE, PLAYER_FRAME_SIZE }      from './Player.js';
import { LAND_TILE, PIXELS_PER_TILE, TILE_IMG, TILE_IMG_RESOURCE } from './Tile.js';

import Level       from './Level.js';
import LocalInput  from './LocalInput.js';
import LocalPlayer from './LocalPlayer.js';
import Player      from './Player.js';

export const GRAVITY = 400;

export const CAMERA_ZOOM = 2.0;

export default class GameScene extends Phaser.Scene {
	preload() {
		this.visibleChunks  = [];

		this.load.spritesheet(PLAYER_IMG, PLAYER_IMG_RESOURCE, {
			frameWidth: PLAYER_FRAME_SIZE,
			frameHeight: PLAYER_FRAME_SIZE
		});
		this.load.image(TILE_IMG, TILE_IMG_RESOURCE);
	}

	create() {
		this.createLevel();
		this.createPlayer();
		this.createCamera();
	}

	createPlayer() {
		this.player = new LocalPlayer(new LocalInput(this), this);
		this.player.chunkUpdate = 'this';
	}

	createLevel() {
		this.level = new Level();	
	}
	
	createCamera() {
		this.cameras.main.startFollow(this.player);
		this.cameras.main.setZoom(CAMERA_ZOOM);
	}

	update() {
		this.updatePlayer();
	}

	updatePlayer() {
		this.player.update();

		if (this.player.chunkUpdate) {
			let offset = NEIGHBORS[this.player.chunkUpdate];

			this.player.x -= offset.x * PIXELS_PER_CHUNK;
			this.player.y -= offset.y * PIXELS_PER_CHUNK;
			this.player.chunkX += offset.x;
			this.player.chunkY += offset.y;
			this.player.chunk = this.player.chunk.neighbors[this.player.chunkUpdate];

			while (this.player.chunkX >= this.level.width) {
				this.player.chunkX -= this.level.width;
			}

			while (this.player.chunkY >= this.level.height) {
				this.player.chunkY -= this.level.height;
			}

			while (this.player.chunkX < 0) {
				this.player.chunkX += this.level.width;
			}

			while (this.player.chunkY < 0) {
				this.player.chunkY += this.level.height;
			}

			let oldChunks = this.visibleChunks;
			this.visibleChunks = [];

			for (let neighbor in NEIGHBORS) {
				let newChunk = this.player.chunk.neighbors[neighbor];
				this.visibleChunks.push(newChunk);
				let isInOldChunks = false;
				for (let oldChunk of oldChunks) {
					if (oldChunk === newChunk) {
						newChunk.layer.x -= offset.x * PIXELS_PER_CHUNK;
						newChunk.layer.y -= offset.y * PIXELS_PER_CHUNK;
						isInOldChunks = true;
						break;
					}
				}

				if(!isInOldChunks) {
					let neighborOffset = NEIGHBORS[neighbor];
					this.renderChunk(
						newChunk,
						neighborOffset.x,
						neighborOffset.y
					);
				}
			}

			for (let oldChunk of oldChunks) {
				let isInVisibleChunks = false;
				for (let newChunk of this.visibleChunks) {
					if (oldChunk === newChunk) {
						isInVisibleChunks = true;
						break;
					}
				}

				if (!isInVisibleChunks) {
					oldChunk.collider.destroy();
					oldChunk.layer.destroy();

					delete oldChunk.collider;
					delete oldChunk.layer;
				}
			}

			this.player.chunkUpdate = '';
		}
	}

	renderChunk(chunk, offsetX, offsetY) {
		let chunkData = chunk.getData();
		let map = this.make.tilemap({
			data: chunkData,
			tileWidth:  PIXELS_PER_TILE,
			tileHeight: PIXELS_PER_TILE
		});
		let tileset = map.addTilesetImage(TILE_IMG);
		let layer = map.createLayer(
			0,
			tileset,
			offsetX * PIXELS_PER_CHUNK,
			offsetY * PIXELS_PER_CHUNK
		);
		layer.setCollision(LAND_TILE);
		let collider = this.physics.add.collider(this.player, layer);
		map.collider = collider;
		chunk.layer = layer;
		chunk.collider = collider;
	}
}
